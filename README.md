# Siemens NX Model


## Part naming convension:

To enhance clarity and consistency in the naming conventions for part modeling in assemblies, the following rules should be adhered to:

1. **Capitalization**: The initial letter of each word in the part name must be capitalized.
2. **Underscore Usage**: Spaces are not permitted in part names. Instead, underscores ("_") should be used to separate words.
3. **Version Tracking**: The version of each part is denoted by appending "_v" followed by the assembly iteration number (starting with 1 by default), a period (".") and then the specific part iteration number. 

For example, if you have a part named "Gear Box" in its first iteration for the first assembly, it should be named "Gear_Box_v1.1". If the part undergoes modifications while the assembly remains the same, it would be "Gear_Box_v1.2", and so on. If the assembly iteration changes, the part name would update to "Gear_Box_v2.1", indicating the first iteration of this part in the second assembly version. 

This system ensures a clear, structured approach to part naming, facilitating easier identification and tracking throughout the design and assembly process.

## Naming each component

The name of each component should not stray too far from that given by the STRUCTURAL subsystem. That beign said, the geometry idealization could result in parts beign cobined or abstracted in a way that a new name would be more appropriate. *In that case*, the STRUCTURAL NAME of the part(s) should be mentioned in the part description, resulting document or commented in the Nastran model file.

## Uploading Files

All files in the **SolidWorks** folder should be uploaded as **.SLDPRT** files.

